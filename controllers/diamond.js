// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');

module.exports.index = function(request, reply){
  reply.redirect('/diamond/new')
}

module.exports.showForm = function(request, reply){
  reply('<form method="post" action="/diamond">Diamond width :<br><input required type="number" name="width"><br><input type="submit" value="Submit"></form>')
}

module.exports.generate = function(request, reply){
  var width = request.payload.width
  var isPositive
  width >= 0 ? isPositive = true : isPositive = false
  width = Math.abs(width)
  if (width % 2 == 0) {
    return reply(width + ' is not an odd number <br><a href=\'/diamond\'>Retour</a>')
  } else if (width == 0) {
    return reply('Width is null <br><a href=\'/diamond\'>Retour</a>')
  }
  var i = 0
  var diamond = []
  for (i=0; i < width; i++) {
    for (j=0; j < width; j++) {
      if(j==0) diamond[i] = ''
      if(j < ((width-1)/2)-i || j < ((width-1)/2)-(width-i-1)){ diamond[i] += '.'}
      else if (j <= ((width-1)/2) + i && j <= ((width-1)/2) + (width-1-i)) {diamond[i] += '*'}
      else { diamond[i] += '.' }
    }
  }
  diamond = diamond.join('<br>')
  if (!isPositive) {
    diamond = invert(diamond)
  }
  reply(diamond)
}

function invert(diamond){
  diamond = findAndReplace(diamond, "*", "-")
  diamond = findAndReplace(diamond, ".", "*")
  diamond = findAndReplace(diamond, "-", ".")
  return diamond
}
function findAndReplace(string, target, replacement) {
  var i = 0, length = string.length;
  for (i; i < length; i++) {
      string = string.replace(target, replacement);
  }
  return string;
}
