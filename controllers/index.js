const
  path = require('path')
  mongoConfig = require(path.join(__dirname, '..', 'config')).mongo,
  dns = require('dns'),
  os = require('os'),
  MongoClient = require('mongodb').MongoClient;

let
  ipAdress = null,
  seen = 0,
  mongoStatus = 'not working';

MongoClient.connect(
  `mongodb://${mongoConfig.host}:${mongoConfig.port}/${mongoConfig.database}`, 
  function(err, db) {
    if(err) {
      console.log('Mongo ERR: ', err)
      return;
    }
    mongoStatus = 'working';
    db.close();
  }
);


dns.lookup(os.hostname(), function(err, ip) {
  ipAdress = ip;
})

module.exports = function(request, reply){
  seen++;
  reply(`Server IP is: ${ipAdress} - Seen: ${seen} times - Mongo: ${mongoStatus}`);
}