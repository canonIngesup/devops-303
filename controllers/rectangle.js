// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');

module.exports.basic = function(request, reply){
  points = request.params.barSequences.split(',').map(function(item) {
    return parseInt(item, 10);
  })

  hauteur = Math.max.apply(Math, points)
  largeur = points.length


  var tableau = []
  var colonnes = []

  colonnes[0] = []
  colonnes[0][0] = '+'
  for (i=1; i<hauteur+1; i++) {
    colonnes[0][i] = i
  }
  for (i=1; i<largeur+1; i++) {
    colonnes[i] = []
    colonnes[i][0] = i
  }
  for (i=0; i<largeur; i++) {
    for (j=1; j<hauteur+1; j++) {
      j <= points[i] ? colonnes[i+1][j] = '$' : colonnes[i+1][j] = '&nbsp;'
    }
  }

  for (i=0; i<=hauteur; i++) {
    tableau[i] = []
    for (j=0; j<=largeur; j++) {
      tableau[i][j] = colonnes[j][hauteur-i]
    }
    tableau[i] = tableau[i].join('&#9;')
  }
  reply('<head><style>body {font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;}</style></head><body><pre>' + tableau.join('<br>') +'</pre></body>')
}

module.exports.random = function(request, reply){
  largeur = parseFloat(request.params.largeur)
  hauteur = parseFloat(request.params.hauteur)
  var points = []
  var tableau = []
  var colonnes = []
  for (i=0; i<largeur; i++) {
    points[i] = Math.floor((Math.random() * hauteur) + 1);
  }
  colonnes[0] = []
  colonnes[0][0] = '+'
  for (i=1; i<hauteur+1; i++) {
    colonnes[0][i] = i
  }
  for (i=1; i<largeur+1; i++) {
    colonnes[i] = []
    colonnes[i][0] = i
  }
  for (i=0; i<largeur; i++) {
    for (j=1; j<hauteur+1; j++) {
      j <= points[i] ? colonnes[i+1][j] = '$' : colonnes[i+1][j] = '&nbsp;'
    }
  }

  for (i=0; i<=hauteur; i++) {
    tableau[i] = []
    for (j=0; j<=largeur; j++) {
      tableau[i][j] = colonnes[j][hauteur-i]
    }
    tableau[i] = tableau[i].join('&#9;')
  }
  reply('<head><style>body {font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;}</style></head><body><pre>' + tableau.join('<br>') +'</pre></body>')
}
