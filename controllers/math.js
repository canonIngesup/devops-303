// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom')
const Mongo = require('mongodb')


const MongoClient = Mongo.MongoClient
const url = 'mongodb://localhost:27017/resultDatabase'

module.exports.getAdd = function(request, reply){
  // parseFloat convert anything to either a float number or NaN
  term1 = parseFloat(request.params.term1)
  term2 = parseFloat(request.params.term2)

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }
  var result = Math.round((term1 + term2)*100)/100
  saveHistory(term1, term2, result, "Addition")
  reply(result)
}

module.exports.getMultiply = function(request, reply){
  // parseFloat convert anything to either a float number or NaN
  term1 = parseFloat(request.params.term1)
  term2 = parseFloat(request.params.term2)

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }
  var result = term1 * term2
  saveHistory(term1, term2, result, "Multiplication")
  reply(result)
}

module.exports.getDivide = function(request, reply){
  dividend = parseFloat(request.params.dividend)
  divisor = parseFloat(request.params.divisor)

  if(isNaN(dividend) || isNaN(divisor)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }

  if(divisor === 0) {
    return reply(Boom.badRequest('Divisor can not be 0.'))
  }
  var result = dividend / divisor
  saveHistory(dividend, divisor, result, "Division")
  reply(result)
}

module.exports.getSubstract = function(request, reply){
  term1 = parseFloat(request.params.term1)
  term2 = parseFloat(request.params.term2)

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'))
  }
  var result = term1 - term2
  saveHistory(term1, term2, result, "Soustraction")
  reply(result)
}

function saveHistory(term1, term2, result, type){

MongoClient.connect(url, function (err, db) {
  if (err) {
    console.log('Impossible de se connecté au serveur. Error:', err)
  } else {
    console.log('Connection établie à', url)
    var collection = db.collection('result')
    var resultat = {term1: term1, term2: term2, result: result, type: type}
    collection.insert([resultat], function (err, result) {
      if (err) {
        console.log(err)
      }
      /*else {
        console.log('"_id" are:', result.length, result)
      }*/
    })
  }
})
}

module.exports.getHistory = function(request, reply){

  MongoClient.connect(url, function (err, db) {
    if (err) {
      console.log('Impossible de se connecté au serveur. Error:', err)
    } else {
      console.log('Connection établie à', url)

      var collection = db.collection('result')
      collection.aggregate([{ $project: {
        _id: false,
        term1: true,
        term2: true,
        result: true,
        type: true
      }}], function (err, result) {
        if (err) {
          console.log(err)
        } else if (result.length) {
          //reply(result)
          //reply(result.length)
          var tab = ""
          for (i = 0; i < result.length; i++) {
            tab += "Opérateur 1 : "+result[i].term1+"<br>"+"Opérateur 2 : "+result[i].term2+"<br>"+"Résultat : "+result[i].result+"<br>"+"Type : "+result[i].type+"<br><br>"
          }
          reply(tab)

        } else {
          console.log('Aucun document trouvé')
        }
      })
    }
  })

}

module.exports.deleteAllHistory = function(request, reply){

  MongoClient.connect(url, function (err, db) {
    if (err) {
      console.log('Impossible de se connecté au serveur. Error:', err)
    } else {
      console.log('Connection établie à', url)

      var collection = db.collection('result')
      collection.remove()
      reply('Delete')
    }
  })
}
