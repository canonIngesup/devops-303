describe("Module Diamond", () => {
  describe("GET /diamond", function() {
    it('should redirect to /diamond/new', function() {
      return server.run({
        method: 'GET',
        url: '/diamond'
      }).then(function(res) {
        res.statusCode.should.equal(302)
      })
    })
  })

  describe("GET /diamond/new", function() {
    it('should show a form with an input and a submit', function() {
      return server.run({
        method: 'GET',
        url: '/diamond/new'
      }).then(function(res){
        res.statusCode.should.equal(200)
        res.result.should.equal('<form method="post" action="/diamond">Diamond width :<br><input required type="number" name="width"><br><input type="submit" value="Submit"></form>')
      })
    })
  })

  describe("POST /diamond", function() {
    // odd = impair
    it('should reject when `width` parameter is not odd', function() {
      return server.run({
        method: 'post',
        url: '/diamond',
        payload: { width: 2 }
      }).then(function(res) {
        res.statusCode.should.equal(200)
        res.result.should.contain("is not an odd number <br><a href='/diamond'>Retour</a>")
      })
    })

    testDiamonds = [{
      width: 1,
      result: '*'
    }, {
      width: 3,
      result: [
        '.*.',
        '***',
        '.*.'
      ].join("<br>")
    }, {
      width: 5,
      result: [
        '..*..',
        '.***.',
        '*****',
        '.***.',
        '..*..'
      ].join("<br>")
    }]

    // C'est possible de créér des tests unitaires de façon dynamique
    testDiamonds.forEach(function(diamond){
      it(`should show correct result on width = ${diamond.width}`, function() {
        return server.run({
          method: 'post',
          url: '/diamond',
          payload: { width: diamond.width }
        }).then(function(res) {
          res.statusCode.should.equal(200)
          res.result.should.equal(diamond.result)
        })
      })
    })

    it('should invert + and . when given `width` is negative', function() {
      return server.run({
        method: 'post',
        url: '/diamond',
        payload: {width: -1}
      }).then(function(res){
        res.statusCode.should.equal(200)
        res.result.should.equal(invert('*'))
      })
    })


    function expectBadRequest(res) {
      res.statusCode.should.equal(400);
      res.result.error.should.equal("Bad Request");
      expect(res.result.error).to.be.equal("Bad Request");
    }

    function findAndReplace(string, target, replacement) {
      var i = 0, length = string.length;
      for (i; i < length; i++) {
          string = string.replace(target, replacement);
      }
      return string;
    }

    function invert(diamond){
      diamond = findAndReplace(diamond, "*", "-")
      diamond = findAndReplace(diamond, ".", "*")
      diamond = findAndReplace(diamond, "-", ".")
      return diamond
    }
  })
})
