describe("Module Rectangle", () => {
  describe("GET /rectangle/X,X,...", function() {
    it('should show nothing', function() {
      return server.run('/rectangle/1,foo').then((res) => {
        res.statusCode.should.equal(200)
        res.result.should.equal('<head><style>body {font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;}</style></head><body><pre></pre></body>')
      })
    })
    it('should show a basic square', function() {
      return server.run('/rectangle/1').then((res) => {
        res.statusCode.should.equal(200)
        res.result.should.equal('<head><style>body {font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;}</style></head><body><pre>1&#9;$<br>+&#9;1</pre></body>')
      })
    })
    it('should show a basic rectangle', function() {
      return server.run('/rectangle/1,2').then((res) => {
        res.statusCode.should.equal(200)
        res.result.should.equal('<head><style>body {font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;}</style></head><body><pre>2&#9;&nbsp;&#9;$<br>1&#9;$&#9;$<br>+&#9;1&#9;2</pre></body>')
      })
    })
  })
  describe("GET /rectangle/X/X", function() {
    it('should show nothing', function() {
      return server.run('/rectangle/1/foo').then((res) => {
        res.statusCode.should.equal(200)
        res.result.should.equal('<head><style>body {font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;}</style></head><body><pre></pre></body>')
      })
    })

    it('should show a basic square', function() {
      return server.run('/rectangle/1/1').then((res) => {
        res.statusCode.should.equal(200)
        res.result.should.equal('<head><style>body {font-family: "Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;}</style></head><body><pre>1&#9;$<br>+&#9;1</pre></body>')
      })
    })
  })

  // ---------------------------------------------------------------------------
  // Some methods that will help us with the tests
  // ---------------------------------------------------------------------------
  function expectBadRequest(res) {
    res.statusCode.should.equal(400);
    res.result.error.should.equal("Bad Request");
    expect(res.result.error).to.be.equal("Bad Request");
  }
})
